//go:build stagenet
// +build stagenet

package thorchain

import "gitlab.com/thorchain/thornode/common/cosmos"

func migrateStoreV86(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV88(ctx cosmos.Context, mgr Manager) {}
